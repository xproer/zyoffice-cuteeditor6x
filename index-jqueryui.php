<?php include_once("cuteeditor_files/include_CuteEditor.php") ; ?>
<html>	
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="php.css"  type="text/css" />
        <link type="text/css" rel="Stylesheet" href="demo.css" />
		<link type="text/css" rel="Stylesheet" href="zyoffice/js/jquery-ui-1.8.4/themes/base/jquery-ui.css" />
    <script type="text/javascript" src="zyoffice/js/json2.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="zyoffice/js/jquery-1.4.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="zyoffice/js/jquery-ui-1.8.4/ui/jquery-ui.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="zyoffice/js/w.jqueryui.js" charset="utf-8"></script>
	<script type="text/javascript" src="demo.js" charset="utf-8"></script>
	</head>
    <body>
		
		<form name="theForm" action="Get_HtmlContent.php" method="post" ID="Form1">
					
		<h1>Enable All Toolbars</h1>
		
		<p>This example shows you all the predefined buttons. </p>
		<br />
        <?php
            $editor=new CuteEditor();
            $editor->ID="Editor1";
            $editor->Text="zyOffice for CuteEditor php";
            $editor->EditorBodyStyle="font:normal 12px arial;";
            $editor->EditorWysiwygModeCss="php.css";
			$editor->CustomAddons = "<img title=\"导入Word文档（docx格式）\" class=\"CuteEditorButton\" onmouseover=\"CuteEditor_ButtonCommandOver(this)\" onmouseout=\"CuteEditor_ButtonCommandOut(this)\" onmousedown=\"CuteEditor_ButtonCommandDown(this)\" onmouseup=\"CuteEditor_ButtonCommandUp(this)\" ondragstart=\"CuteEditor_CancelEvent()\" Command=\"zyOffice\" src=\"zyoffice/css/w.png\" />";
            $editor->Draw();
			$ClientID = $editor->ClientID();
            $editor=null;
            
            //use $_POST["Editor1"]to retrieve the data
        ?>
		<div id="demos"></div>			
		<script type="text/javascript">
            zyOffice.getInstance({
            	api: "http://localhost:13710/zyoffice/word/convert"				
			});//加载控件
			
            var edt = document.getElementById('<?php echo $ClientID;?>');
    
            $(document).ready(function()
            {
                zyOffice.getInstance().SetEditor(edt);
            });
			
			function CuteEditor_OnCommand(editor,command,ui,value)    
			{    
				//handle the command by yourself    
				if(command=="zyOffice")
				{    
					zyOffice.getInstance().api.openDoc();
					return true;    
				}    
			}    
        </script>
						
		</form>
	</body>
</html>