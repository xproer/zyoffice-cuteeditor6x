﻿/*
	版权所有 2009-2024 荆门泽优软件有限公司 保留所有版权。
    版本：1.0
    更新记录：
        2024-03-07 创建
*/
function zyOfficeManager()
{
    //url=>res/
    //http://localhost:8888/zyoffice/js/w.js=>
    this.getJsDir = function () {
        var js = document.scripts;
        var jsPath;
        for (var i = 0; i < js.length; i++) {
            if (js[i].src.indexOf("js/w.jqueryui.js") > -1) {
                jsPath = js[i].src.substring(0, js[i].src.indexOf("js/w.jqueryui.js"));
            }
        }
        return jsPath;
    };
    var rootDir = this.getJsDir();
    //http://localhost/WordPaster/css/
    var pathRes = rootDir + "css/";

    var _this = this;
    this.Editor = null;
    this.event={
        scriptReady: function () {
            $(function () {
                //加载
                if (typeof (_this.ui.render) == "undefined") {
                    _this.LoadTo($(document.body));
                }
                else if (typeof (_this.ui.render) == "string") {
                    _this.LoadTo($("#" + _this.ui.render));
                }
                else if (typeof (_this.ui.render) == "object") {
                    _this.LoadTo(_this.ui.render);
                }
            });
        },
        doc_sel: function (evt) {
            _this.ui.dlg.panel.css("display", "");
            _this.ui.dlg.msg.text("正在转换中");
            _this.api.openDialog();
            var fs = evt.target.files;
            _this.api.upload_doc(fs[0]);
            evt.target.value = null;
        },
        upload_complete: function (evt) {
            var svr = JSON.parse(evt.target.responseText);
            _this.api.insertHtml(svr.body);
            _this.ui.dlg.panel.dialog('close');
        },
        upload_failed: function () {
            _this.ui.dlg.ico.attr("src", _this.ui.ico.error);
            _this.ui.dlg.msg.text("服务器连接失败，请检查Office文件转换服务是否启动");
        },
        upload_process: function (evt) { }
    };
    this.ui = { setup: null ,
        single: null,
        btn: { up: null },
        dlg: { panel: null, ico: null, msg: null },
        ico:{
            error:pathRes+"error.png",
            upload:pathRes+"upload.gif"
        }
    };
    this.data={
        browser:{name:navigator.userAgent.toLowerCase(),ie:true,ie64:false,chrome:false,firefox:false,edge:false,arm64:false,mips64:false,platform:window.navigator.platform.toLowerCase()},
        error:{
        "0": "连接服务器错误",
        "1": "发送数据错误",
        "2": "接收数据错误"
      },
      type:{local:0/*本地图片*/,network:1/*网络图片*/,word:2/*word图片*/},
      jsCount: 0,//已经加载的脚本总数
    };
    this.api = {
        openDoc: function () {
            _this.ui.btn.up.click();
        },
        openDialog: function () {
            _this.ui.dlg.panel.dialog({
                title: "Office文档转换",
                width: "550",
                height:"160",
                close: function (e, ui) {
                    _this.ui.dlg.panel.css("display", "none");
                    _this.ui.dlg.ico.attr("src", _this.ui.ico.upload);
                }
            });
        },
        upload_doc: function (file) {
            var fileObj = file;

            var form = new FormData(); // FormData object
            form.append("file", fileObj); // File object

            var xhr = new XMLHttpRequest();  // XMLHttpRequest object
            xhr.open("post", _this.Config.api, true); //post
            xhr.onload = _this.event.upload_complete;
            xhr.onerror = _this.event.upload_failed;

            xhr.upload.onprogress = _this.event.upload_process;
            xhr.upload.onloadstart = function () {
                ot = new Date().getTime();
                oloaded = 0;
            };

            xhr.send(form);
        },
        insertHtml: function (v) {
            _this.Editor.ExecCommand("PasteHTML", false, v)
        }
    };
    
	this.Config = {
        "api":"http://localhost:8080/zyoffice/word/convert"
    };

    if (arguments.length > 0) {
        var cfg = arguments[0];
        if (typeof (cfg) != "undefined") $.extend(true, this.Config, cfg);
        if (typeof (cfg.ui) != "undefined") $.extend(true, this.ui, cfg.ui);
    }

	this.LoadTo = function (o)
    {
        o.append('<input style="display: none" type="file" id="btnUpWord" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document"/>\
<div id="w_dlg" style="display:none">\
            <img id = "w_ico" />\
                    <span id="w_msg"></div>\
</div>');
        this.ui.btn.up = $("#btnUpWord");
        this.ui.btn.up.change(this.event.doc_sel);
        this.ui.dlg.panel = $("#w_dlg");
        this.ui.dlg.ico = $("#w_ico");
        this.ui.dlg.ico.attr("src", this.ui.ico.upload);
        this.ui.dlg.msg = $("#w_msg");
	};

	this.InsertHtml = function (html)
	{
	    //this.Editor.insertHtml(html);
	    _this.Editor.ExecCommand("PasteHTML", false, html)
	};
	this.GetEditor = function () { return this.Editor; };

    //在FCKeditor_OnComplete()中调用
	this.SetEditor = function (edt)
	{
	    _this.Editor = edt;
	};

    //init
    this.event.scriptReady();
}

var zyOffice = {
    instance: null,
    inited: false,
    getInstance: function (cfg) {
        if (this.instance == null) {
            this.instance = new zyOfficeManager(cfg);
            window.zyoffice = zyOffice;
        }
        return this.instance;
    }
}